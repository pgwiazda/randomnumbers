﻿using System;

namespace RandomNumbers.Dtos
{
    public class CurrentMatch
    {
        public string Name { get; set; }
        public DateTime ExpiryTimestamp { get; set; }
    }
}
