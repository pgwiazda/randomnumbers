﻿namespace RandomNumbers.Dtos
{
    public class FinishedMatch
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Winner { get; set; }
    }
}
