﻿namespace RandomNumbers.CustomExceptions
{
    public class NoActiveMatchException : AppException
    {
        public override string Code { get; } = "no_active_matches";
        public NoActiveMatchException() : base($"There are no active matches currently.")
        {
        }
    }
}
