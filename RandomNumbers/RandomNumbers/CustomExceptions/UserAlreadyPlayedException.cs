﻿namespace RandomNumbers.CustomExceptions
{
    public class UserAlreadyPlayedException : AppException
    {
        public override string Code { get; } = "user_already_played_exception";
        public UserAlreadyPlayedException() : base($"User already played this match.")
        {
        }
    }
}
