﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RandomNumbers.CustomExceptions
{
    public class TimeExpiredException : AppException
    {
        public override string Code { get; } = "match_has_already_finished";
        public TimeExpiredException() : base($"Match has already finished.")
        {
        }
    }
}
