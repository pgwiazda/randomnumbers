import React, { Component, useEffect, useState } from "react";
import styled from "styled-components";
import authService from "./api-authorization/AuthorizeService";

export function Matches() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [matches, setMatches] = useState(null);
  const [currentMatch, setCurrentMatch] = useState(null);
  const [playedMatch, setPlayedMatch] = useState(null);
  const [currentScore, setCurrentScore] = useState(null);

  useEffect(() => {
    async function getAuthenticationState() {
      const authenticated = await authService.isAuthenticated();
      setIsAuthenticated(authenticated);
    }

    getAuthenticationState();
  }, []);

  useEffect(() => {
    async function getMatches() {
      getPlayedMatches();
      const token = await authService.getAccessToken();
      if (token) {
        const currentMatchResponse = await fetch("matches/current", {
          headers: { Authorization: `Bearer ${token}` },
        });
        const currentMatch = await currentMatchResponse.json();
        setCurrentMatch(currentMatch);
      }
    }

    getMatches();
  }, []);

  async function getPlayedMatches() {
    const token = await authService.getAccessToken();
    const response = await fetch("matches", {
      headers: !token ? {} : { Authorization: `Bearer ${token}` },
    });
    const data = await response.json();
    setMatches(data);
  }

  async function playMatch() {
    if (isAuthenticated) {
      const token = await authService.getAccessToken();
      const response = await fetch("matches/play", {
        method: "POST",
        headers: { Authorization: `Bearer ${token}` },
      });

      const score = await response.json();
      setCurrentScore(score);
      setPlayedMatch(currentMatch.name);
    }
  }

  async function refresh() {
    getPlayedMatches();
  }

  return (
    <>
      <Title>Matches</Title>
      <RefreshButton onClick={refresh}>Refresh</RefreshButton>
      <Container>
        {matches &&
          matches.map((m) => (
            <Card key={m.name}>
              <MatchName>{m.name}</MatchName>
              <MatchWinner>{m.winner}</MatchWinner>
            </Card>
          ))}
      </Container>

      {isAuthenticated ? (
        <CurrentMatchContainer>
          <ExpirationDate>
            {currentMatch && currentMatch.expiryTimestamp}
          </ExpirationDate>
          {playedMatch ? (
            <Score>{currentScore}</Score>
          ) : (
            <PlayButton onClick={playMatch}>Play now</PlayButton>
          )}
        </CurrentMatchContainer>
      ) : null}
    </>
  );
}

const Title = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 4rem;
`;

const Container = styled.div`
  margin-top: 40px;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: auto;
  grid-gap: 1rem;
`;

const Card = styled.div`
  display: flex;
  align-items: space-between;
  justify-content: center;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  padding: 2px 16px;
  width: 120px;
  height: 160px;
`;
const MatchName = styled.span``;

const MatchWinner = styled.span``;

const CurrentMatchContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 50px;
`;

const ExpirationDate = styled.span``;

const Score = styled.span`
  font-size: 2rem;
  margin-left: 20px;
`;

const PlayButton = styled.button`
  background-color: #4caf50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin-left: 20px;
`;

const RefreshButton = styled.button`
  background-color: #4caf50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
`;
