﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RandomNumbers.Dtos;
using RandomNumbers.Models;

namespace RandomNumbers.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MatchesController : ControllerBase
    {
        private readonly ILogger<MatchesController> _logger;
        private readonly MatchesAggregate _aggregate;

        public MatchesController(ILogger<MatchesController> logger, MatchesAggregate aggregate)
        {
            _logger = logger;
            _aggregate = aggregate;
        }

        [HttpGet]
        public IEnumerable<FinishedMatch> Get()
        {
            return _aggregate.GetFinishedMatches();
        }

        [Authorize]
        [HttpGet("current")]
        public CurrentMatch GetCurrentMatch()
        {
            return _aggregate.GetCurrentMatch();
        }

        [Authorize]
        [HttpPost("play")]
        public int Play()
        {
            var user = this.User;
            var score = _aggregate.PlayMatch(user.FindFirst(ClaimTypes.NameIdentifier).Value);

            return score;
        }
    }
}
