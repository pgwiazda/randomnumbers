﻿using System;
using System.Collections.Generic;
using System.Linq;
using RandomNumbers.CustomExceptions;
using RandomNumbers.Dtos;

namespace RandomNumbers.Models
{
    public class MatchesAggregate
    {
        public IEnumerable<Match> Matches { get; private set; }

        public MatchesAggregate()
        {
            Matches = PredefinedMatches;
        }

        public IEnumerable<FinishedMatch> GetFinishedMatches()
        {
            var currentTime = DateTime.UtcNow;
            var finishedMatches = Matches.Where(x => x.ExpiryTimestamp < currentTime);

            var mappedMatches = finishedMatches.Select(x => new FinishedMatch()
            {
                Id = x.Id.ToString(),
                Name = x.Name,
                Winner = x.GetWinnerName()
            }).ToList();
            return mappedMatches;
        }

        public CurrentMatch GetCurrentMatch()
        {
            var currentMatch = GetActiveMatch();

            if (currentMatch != null)
            {
                return new CurrentMatch()
                {
                    ExpiryTimestamp = currentMatch.ExpiryTimestamp,
                    Name = currentMatch.Name
                };
            }

            return null;
        }

        public int PlayMatch(string user)
        {
            var currentMatch = GetActiveMatch();

            if (currentMatch == null)
            {
                throw new NoActiveMatchException();
            }

            var (match, score) = currentMatch.Play(user);
            Matches = Matches.Select(x => x == currentMatch ? match : x);
            return score;
        }

        private static readonly Match[] PredefinedMatches = {
            new Match("match 1", DateTime.UtcNow.AddMinutes(5)),
            new Match("match 2", DateTime.UtcNow.AddMinutes(2))
        };

        private Match GetActiveMatch()
        {
            var currentTime = DateTime.UtcNow;

            var activeMatches = Matches.Where(x => x.ExpiryTimestamp > currentTime);
            var currentMatch = activeMatches.OrderBy(x => x.ExpiryTimestamp).FirstOrDefault();

            return currentMatch;
        }
    }
}
