﻿using System;
using System.Collections.Generic;
using RandomNumbers.CustomExceptions;

namespace RandomNumbers.Models
{
    public class Match
    {
        public Guid Id { get; }
        public string Name { get; }
        public DateTime ExpiryTimestamp { get; }
        public int CurrentHighScore { get; set; } = -1;
        private Dictionary<string, int> PlayersScores { get; set; } = new Dictionary<string, int>();
        private string Winner { get; set; }

        public Match(string name, DateTime expiryTimestamp)
        {
            Id = Guid.NewGuid();
            Name = name;
            ExpiryTimestamp = expiryTimestamp;
        }

        public (Match, int) Play(string user)
        {
            if (ExpiryTimestamp < DateTime.UtcNow)
            {
                throw new TimeExpiredException();
            }

            if (PlayersScores.ContainsKey(user))
            {
                throw new UserAlreadyPlayedException();
            }

            var random = new Random();
            int generatedNumber = random.Next(0, 101);

            if (generatedNumber > CurrentHighScore)
            {
                CurrentHighScore = generatedNumber;
                Winner = user;
            }

            PlayersScores.Add(user, generatedNumber);

            return (this, generatedNumber);
        }

        public string GetWinnerName()
        {
            return ExpiryTimestamp < DateTime.UtcNow ? Winner : "";
        }

    }
}
